pub mod helper;
pub mod source;

#[macro_use]
extern crate nickel;

use nickel::{Nickel, Options};

fn main() {
    let addr = helper::query_port();

    let mut server = Nickel::new();
    server.options = Options::default().output_on_listen(false);

    source::init(&mut server);
    println!(
        "server is running at http://{}",
        console::style(&addr).red()
    );
    //static_server(&mut server);

    server.listen(&addr).unwrap();
}
